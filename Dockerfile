FROM alpine

WORKDIR /usr/share/java/apache-ant/lib/
RUN wget http://search.maven.org/remotecontent?filepath=org/apache/ivy/ivy/2.5.0/ivy-2.5.0.jar

RUN apk update && apk add openjdk8 && apk add apache-ant

WORKDIR /myapp
COPY . .

ENTRYPOINT [ "ant" ]
CMD [ "all" ]