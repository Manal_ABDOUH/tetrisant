package fr.ubo.tetris;

import org.junit.Test;

import static org.junit.Assert.*;

public class MyClassTest {

    @Test
    public void testQuotidient() {
        MyClass tester = new MyClass();
        assertEquals(2, tester.quotidient(10, 5));
        //assertEquals("10 / 5 must be 2", tester.quotidient(10, 5));
    }
}